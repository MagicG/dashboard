export class DashboardMenuItem {

  public label: string;
  public route: string;

  constructor(label: string, route: string) {
    this.label = label;
    this.route = route;
  }

}

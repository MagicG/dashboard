import {Routes} from '@angular/router';
import {NgBootstrapMainComponent} from './ng-bootstrap-main/ng-bootstrap-main.component';

export const NG_BOOTSTRAP_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: NgBootstrapMainComponent,
    children: []
  }
];

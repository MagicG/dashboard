import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomSecondComponent } from './custom-second.component';

describe('CustomSecondComponent', () => {
  let component: CustomSecondComponent;
  let fixture: ComponentFixture<CustomSecondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomSecondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomSecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

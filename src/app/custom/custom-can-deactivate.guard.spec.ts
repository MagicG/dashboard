import { TestBed, async, inject } from '@angular/core/testing';

import { CustomCanDeactivateGuard } from './custom-can-deactivate.guard';

describe('CustomCanDeactivateGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomCanDeactivateGuard]
    });
  });

  it('should ...', inject([CustomCanDeactivateGuard], (guard: CustomCanDeactivateGuard) => {
    expect(guard).toBeTruthy();
  }));
});

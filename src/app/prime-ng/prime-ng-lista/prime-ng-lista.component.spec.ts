import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeNgListaComponent } from './prime-ng-lista.component';

describe('PrimeNgListaComponent', () => {
  let component: PrimeNgListaComponent;
  let fixture: ComponentFixture<PrimeNgListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimeNgListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeNgListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

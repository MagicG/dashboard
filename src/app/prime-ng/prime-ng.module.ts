import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PrimeNgMainComponent} from './prime-ng-main/prime-ng-main.component';
import {PrimeNgRoutingModule} from './prime-ng-routing.module';
import { PrimeNgDataComponent } from './prime-ng-data/prime-ng-data.component';
import { PrimeNgListaComponent } from './prime-ng-lista/prime-ng-lista.component';

@NgModule({
  declarations: [PrimeNgMainComponent, PrimeNgDataComponent, PrimeNgListaComponent],
  imports: [
    CommonModule,
    PrimeNgRoutingModule
  ]
})
export class PrimeNGModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeNgMainComponent } from './prime-ng-main.component';

describe('PrimeNgMainComponent', () => {
  let component: PrimeNgMainComponent;
  let fixture: ComponentFixture<PrimeNgMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimeNgMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeNgMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

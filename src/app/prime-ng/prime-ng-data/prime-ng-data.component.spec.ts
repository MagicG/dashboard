import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeNgDataComponent } from './prime-ng-data.component';

describe('PrimeNgDataComponent', () => {
  let component: PrimeNgDataComponent;
  let fixture: ComponentFixture<PrimeNgDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimeNgDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeNgDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

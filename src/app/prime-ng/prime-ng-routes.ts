import {Routes} from '@angular/router';
import {PrimeNgMainComponent} from './prime-ng-main/prime-ng-main.component';
import {PrimeNgDataComponent} from './prime-ng-data/prime-ng-data.component';

export const PRIME_NG_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: PrimeNgMainComponent,
    children: [
      {
        path: 'prime-ng-data',
        component: PrimeNgDataComponent,
        data: {title: 'Title: LABORATORIO 3', content: 'the data was received from router and it`s working'}
      },
      {
        path: '',
        redirectTo: '/prime-ng/prime-ng-data',
        pathMatch: 'full'
      }
    ]
  }
];
